<?php
include_once("../includes/dataaccess/OrderDataAccess.inc.php");
include_once("../includes/models/Order.inc.php");
include_once("create-test-database.inc.php");


$testResults = array();

// You'll have to run all these tests for each of your data access classes
testConstructor(); // we should test the constructor, but maybe we'll skip this one in the interest of time
testConvertModelToRow();
testConvertRowToModel();
testGetAll();
testGetById();
testInsert(); 
testUpdate(); 
// testDelete(); 

echo(implode("<br>",$testResults));


function testConstructor(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING constructor...</b>";

	// TEST - create an instance of the ConcactDataAccess class
	$da = new OrderDataAccess($link);
	
	if($da){
		$testResults[] = "PASS - Created instance of OrderDataAccess";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of OrderDataAccess";
	}

	// Test - an exception should be thrown if the $link param is not a valid link
	try{
		$da = new OrderDataAccess("BLAHHHHHH");
		$testResults[] = "FAIL - Exception is NOT thrown when link param is invalid";
	}catch(Exception $e){
		$testResults[] = "PASS - Exception is thrown when link param is invalid";
	}
}

function testConvertModelToRow(){

	global $testResults, $link;

	$testResults[] = "<b>TESTING convertModelToRow()...</b>";

	$da = new OrderDataAccess($link);

	$options = array(
		'id' => 1,
		'status' => "Test Order",
		'userId' => "1"
	);

	$o = new Orders($options);

	$expectedResult = array(
		'order_id' => 1,
		'order_status' => "Test Order", 
		'user_id' => '1'
	);
	
	$actualResult = $da->convertModelToRow($o);

	/*
	var_dump($expectedResult);
	var_dump($actualResult);
	die();
	*/

	if(empty(array_diff_assoc($expectedResult, $actualResult))){
		$testResults[] = "PASS - Converted Order to proper assoc array";
	}else{
		$testResults[] = "FAIL - DID NOT convert Order to proper assoc array";
	}
}


function testConvertRowToModel(){
	global $testResults, $link;

	$testResults[] = "<b>TESTING convertRowToModel()...</b>";

	$da = new OrderDataAccess($link);

	// Create an assoc array that simulates the data coming from a query
	$row = array('order_id' => 1, 'order_status' => "Test Order", 'user_id' => "1");
	
	$actualResult = $da->convertRowToModel($row);
	
	// Create a Order model that is equivalent to what we expect to be returned by convertRowToModel
	$expectedResult = new Orders([
		'id' => 1,
		'status' => "Test Order",
		'userId' => "1"
	]);

	if($actualResult->equals($expectedResult)){
		$testResults[] = "PASS - Converted row (assoc array) to Order";
	}else{
		$testResults[] = "FAIL - DID NOT Convert row (assoc array) to Order";
	}
}


function testGetAll(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getAll()...</b>";

	$da = new OrderDataAccess($link);
	$actualResult = $da->getAll();

	//$testResults[] = print_r($actualResult, true);
	//var_dump($actualResult);die();
}


function testGetById(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getById()...</b>";

	// We need an instance of a OrderDataAccess object so that we can call the method we want to test
	$da = new OrderDataAccess($link);
	$actualResult = $da->getById(1);

	//var_dump($actualResult);die();

}

function testInsert(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING insert()...</b>";

	$da = new OrderDataAccess($link);

	$options = array(
		'status' => "NEW Test Order",
		'userId' => "1"
	);

	$actualResult = $da->insert(new Orders($options));
	//var_dump($actualResult);die();
}

function testUpdate(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING update()...</b>";

	$da = new OrderDataAccess($link);

	$options = array(
		'id' => 1,
		'status' => "UPDATED Test Order",
		'userId' => "2"
	);

	$actualResult = $da->update(new Orders($options));
	var_dump($actualResult);die();
	
}


?>