<?php
include_once("../includes/models/Role.inc.php");


// we'll use these options to create valid Role in our tests
$options = array(
	'id' => 1,
	'name' => "Test Role",
	'description' => "This is a role for testing"
);

// This array will store the test results
$testResults = array();

// run the test functions
testConstructor();
testIsValid();
testToJSON();
//testToCSV();
testToXML();
testToArray();

echo(implode("<br>",$testResults));



function testConstructor(){
	global $testResults, $options;
	$testResults[] = "<b>Testing constructor...</b>";

	// TEST - Make sure we can create a Role object
	$r = new Role();
	
	if($r){
		$testResults[] = "PASS - Created instance of Role model object";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of a Role model object";
	}

	// TEST - Make sure each property gets set correctly
	$r = new Role($options);

	if($r->id === 1){
		$testResults[] = "PASS - Set id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set id properly";
	}

	if($r->name == "Test Role"){
		$testResults[] = "PASS - Set Name properly";
	}else{
		$testResults[] = "FAIL - DID NOT set Name properly";
	}
	
	if($r->description == "This is a role for testing"){
		$testResults[] = "PASS - Set role properly";
	}else{
		$testResults[] = "FAIL - DID NOT set role properly";
	}

}


function testIsValid(){
	global $testResults, $options;
	$testResults[] = "<b>Testing isValid()...</b>";

	$r = new Role($options);

	// isValid() should return false when the ID is not numeric
	$r->id = "";

	if($r->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is not numeric";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is not numeric";
	}

	// isValid() should return false when the ID is a negative number
	$r->id = -1;
	if($r->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is a negative number";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is a negative number";
	}

	// If the ID is not valid, then the validation errors array should include an 'id' key
	$errors = $r->getValidationErrors();
	if(isset($errors['id'])){
		$testResults[] = "PASS - validationErrors includes key for ID";
	}else{
		$testResults[] = "FAIL - validationErrors does NOT include key for ID";
	}


	// When the role name is longer than 30 characters, isValid() should return false
	$r = new Role($options);
	$r->name = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
	if($r->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when role name is too long";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when role name is too long";
	}

	// When the role name is longer than 30 characters, the error message should be 'Role name must be 30 characters or less'
	$errors = $r->getValidationErrors();
	if(isset($errors['name']) && $errors['name'] == "Role name must be 30 characters or less"){
		$testResults[] = "PASS - Validation message is 'Role name must be 30 characters or less'";
	}else{
		$testResults[] = "FAIL - Validation message is NOT 'Role name must be 30 characters or less'";
	}

	
	// the description must be less than 200 characters
	$r = new Role($options);
	$r->description = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
					  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
					  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
					  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
					  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

	if($r->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when Description is too long";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when Description is too long";
	}

	// When the role description is longer than 200 characters, the error message should be 'Role description must be 200 characters or less'
	$errors = $r->getValidationErrors();
	if(isset($errors['description']) && $errors['description'] == "Role description must be 200 characters or less"){
		$testResults[] = "PASS - Validation message is 'Role description must be 200 characters or less'";
	}else{
		$testResults[] = "FAIL - Validation message is NOT 'Role description must be 200 characters or less'";
	}


	// the description can be empty
	$r = new Role($options);
	$r->description = "";

	if($r->isValid() === true){
		$testResults[] = "PASS - isValid() returns true when description is empty";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return true when description is empty";
	}


	// isValid() returns true when everything is valid
	$r = new Role($options);
	$r->description = "";

	if($r->isValid() === true){
		$testResults[] = "PASS - isValid() returns true when everything is valid";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return true when everything is valid";
	}

	// when isValid returns true, there should be no validation error
	$errors = $r->getValidationErrors();
	if(empty($errors)){
		$testResults[] = "PASS - There are no validation messages when isValid() returns true";
	}else{
		$testResults[] = "FAIL - There ARE validation messages when isValid() returns true";
	}
	
	
}


function testToJSON(){

	global $testResults, $options;
	$testResults[] = "<b>Testing toJSON()...</b>";

	$r = new Role($options);

	$expectedResult = '{"id":1,"name":"Test Role","description":"This is a role for testing"}';
	$actualResult = $r->toJSON();

	if($expectedResult == $actualResult){
		$testResults[] = "PASS - converted to JSON properly";
	}else{
		$testResults[] = "FAIL - DID NOT convert to JSON properly";
	}
}


function testToCSV(){

	global $testResults, $options;
	$testResults[] = "<b>Testing toCSV()...</b>";

	$r = new Role($options);

	$expectedResult = "1,Test Role,This is a role for testing";
	$actualResult = $r->toCSV();

	if($expectedResult == $actualResult){
		$testResults[] = "PASS - converted to CSV properly";
	}else{
		$testResults[] = "FAIL - DID NOT convert to CSV properly";
	}
	

}


function testToXML(){

	global $testResults, $options;
	$testResults[] = "<b>Testing toXML()...</b>";

	$r = new Role($options);
	
	$expectedResult = "<role><id>1</id><name>Test Role</name><description>This is a role for testing</description></role>";
	$actualResult = $r->toXML();

	if($expectedResult == $actualResult){
		$testResults[] = "PASS - converted to XML properly";
	}else{
		$testResults[] = "FAIL - DID NOT convert to XML properly";
	}
	

}


function testToArray(){
	global $testResults, $options;
	$testResults[] = "<b>Testing toArray()...</b>";

	$r = new Role($options);
	$diff = array_diff_assoc($r->toArray(), $options);
	// array_diff_assoc() returns the differences between two arrays
	// it it's empty, then the two arrays have the same keys and values
	if(empty($diff)){
		$testResults[] = "PASS - converted to array properly";
	}else{
		$testResults[] = "FAIL - DID NOT convert to array properly";
	}
}
