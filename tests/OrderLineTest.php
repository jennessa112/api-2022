<?php
include_once("../includes/models/OrderLine.inc.php");


// we'll use these options to create valid Order Line in our tests
$options = array(
	'id' => 1,
	'productId' => 2,
    'orderId' => 1
);

// This array will store the test results
$testResults = array();

// run the test functions
testConstructor();
testIsValid();

echo(implode("<br>",$testResults));



function testConstructor(){
	global $testResults, $options;
	$testResults[] = "<b>Testing constructor...</b>";

	// TEST - Make sure we can create a Role object
	$o = new OrderLine();
	
	if($o){
		$testResults[] = "PASS - Created instance of Order Line model object";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of a Order Line model object";
	}

	// TEST - Make sure each property gets set correctly
	$o = new OrderLine($options);

	if($o->id === 1){
		$testResults[] = "PASS - Set id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set id properly";
	}

	if($o->productId == '2'){
		$testResults[] = "PASS - Set product id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set product id properly";
	}
	
	if($o->orderId == '1'){
		$testResults[] = "PASS - Set order id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set order id properly";
	}

}


function testIsValid(){
	global $testResults, $options;
	$testResults[] = "<b>Testing isValid()...</b>";

	$o = new OrderLine($options);

	// isValid() should return false when the ID is not numeric
	$o->id = "";

	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is not numeric";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is not numeric";
	}

	// isValid() should return false when the ID is a negative number
	$o->id = -1;
	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is a negative number";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is a negative number";
	}

	// If the ID is not valid, then the validation errors array should include an 'id' key
	$errors = $o->getValidationErrors();
	if(isset($errors['id'])){
		$testResults[] = "PASS - validationErrors includes key for ID";
	}else{
		$testResults[] = "FAIL - validationErrors does NOT include key for ID";
	}
	
	$o = new OrderLine($options);
	$o->productId = "";

	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when the product id is not valid";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when the product id is not valid";
	}

	$errors = $o->getValidationErrors();
	if(isset($errors['productId']) && $errors['productId'] == "The product id is not valid"){
		$testResults[] = "PASS - Validation message is 'The product is not valid'";
	}else{
		$testResults[] = "FAIL - Validation message is NOT 'The product id is not valid'";
	}

	$o = new OrderLine($options);
	$o->orderId = "";

	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when the order id is not valid";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when the order id is not valid";
	}

	// When the roleId is not valid, the error message should be 'The user is not valid'
	$errors = $o->getValidationErrors();
	if(isset($errors['orderId']) && $errors['orderId'] == "The order id is not valid"){
		$testResults[] = "PASS - Validation message is 'The order id is not valid'";
	}else{
		$testResults[] = "FAIL - Validation message is NOT 'The order id is not valid'";
	}


	// When the order is valid, isValid() should return true
	$o = new OrderLine($options);
	
	if($o->isValid() === true){
		$testResults[] = "PASS - isValid() returns true when everything is valid";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return true when everything is valid";
	}

	// when isValid returns true, there should be no validation error
	$errors = $o->getValidationErrors();
	if(empty($errors)){
		$testResults[] = "PASS - There are no validation messages when isValid() returns true";
	}else{
		$testResults[] = "FAIL - There ARE validation messages when isValid() returns true";
	}
	
	
}
