<?php
include_once("../includes/models/Order.inc.php");


// we'll use these options to create valid Order in our tests
$options = array(
	'id' => 1,
	'status' => "Completed",
    'userId' => 1
);

// This array will store the test results
$testResults = array();

// run the test functions
testConstructor();
testIsValid();

echo(implode("<br>",$testResults));



function testConstructor(){
	global $testResults, $options;
	$testResults[] = "<b>Testing constructor...</b>";

	// TEST - Make sure we can create a Role object
	$o = new Orders();
	
	if($o){
		$testResults[] = "PASS - Created instance of Order model object";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of a Order model object";
	}

	// TEST - Make sure each property gets set correctly
	$o = new Orders($options);

	if($o->id === 1){
		$testResults[] = "PASS - Set id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set id properly";
	}

	if($o->status == 'Completed'){
		$testResults[] = "PASS - Set status properly";
	}else{
		$testResults[] = "FAIL - DID NOT set status properly";
	}
	
	if($o->userId == "1"){
		$testResults[] = "PASS - Set user id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set user id properly";
	}

}


function testIsValid(){
	global $testResults, $options;
	$testResults[] = "<b>Testing isValid()...</b>";

	$o = new Orders($options);

	// isValid() should return false when the ID is not numeric
	$o->id = "";

	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is not numeric";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is not numeric";
	}

	// isValid() should return false when the ID is a negative number
	$o->id = -1;
	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is a negative number";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is a negative number";
	}

	// If the ID is not valid, then the validation errors array should include an 'id' key
	$errors = $o->getValidationErrors();
	if(isset($errors['id'])){
		$testResults[] = "PASS - validationErrors includes key for ID";
	}else{
		$testResults[] = "FAIL - validationErrors does NOT include key for ID";
	}

    $o = new Orders($options);
	$o->status = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when order status is too long";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when order status is too long";
	}

	// When the role name is longer than 30 characters, the error message should be 'Role name must be 30 characters or less'
	$errors = $o->getValidationErrors();
	if(isset($errors['status']) && $errors['status'] == "Order status must be 30 characters or less"){
		$testResults[] = "PASS - Validation message is 'Order status must be 30 characters or less'";
	}else{
		$testResults[] = "FAIL - Validation message is NOT 'Order status must be 30 characters or less'";
	}
	
	
	$o = new Orders($options);
	$o->userId = "";

	if($o->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when the userId is not valid";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when the userId is not valid";
	}

	// When the roleId is not valid, the error message should be 'The user is not valid'
	$errors = $o->getValidationErrors();
	if(isset($errors['userId']) && $errors['userId'] == "The user is not valid"){
		$testResults[] = "PASS - Validation message is 'The user is not valid'";
	}else{
		$testResults[] = "FAIL - Validation message is NOT 'The user is not valid'";
	}


	// When the order is valid, isValid() should return true
	$o = new Orders($options);
	
	if($o->isValid() === true){
		$testResults[] = "PASS - isValid() returns true when everything is valid";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return true when everything is valid";
	}

	// when isValid returns true, there should be no validation error
	$errors = $o->getValidationErrors();
	if(empty($errors)){
		$testResults[] = "PASS - There are no validation messages when isValid() returns true";
	}else{
		$testResults[] = "FAIL - There ARE validation messages when isValid() returns true";
	}
	
	
}
