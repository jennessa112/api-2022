<?php
include_once("../includes/dataaccess/OrderLineDataAccess.inc.php");
include_once("../includes/models/OrderLine.inc.php");
include_once("create-test-database.inc.php");


$testResults = array();

// You'll have to run all these tests for each of your data access classes
testConstructor();
testConvertModelToRow();
testConvertRowToModel();
testGetAll();
testGetById();
testInsert(); 
testUpdate(); 
//testDelete(); 

echo(implode("<br>", $testResults));



function testConstructor(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING constructor...</b>";

	// TEST - create an instance of the ConcactDataAccess class
	$da = new OrderLineDataAccess($link);
	
	if($da){
		$testResults[] = "PASS - Created instance of OrderLineDataAccess";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of OrderLineDataAccess";
	}

	// Test - an exception should be thrown if the $link param is not a valid link
	try{
		$da = new OrderLineDataAccess("BLAHHHHHH");
		$testResults[] = "FAIL - Exception is NOT thrown when link param is invalid";
	}catch(Exception $e){
		$testResults[] = "PASS - Exception is thrown when link param is invalid";
	}
}

function testConvertModelToRow(){
	global $testResults, $link;

	$testResults[] = "<b>TESTING convertModelToRow()...</b>";

	$da = new OrderLineDataAccess($link);

	$options = array(
		'id' => 1,
		'orderId' => "1",
		'productId' => "2"
	);

	$o = new OrderLine($options);

	$expectedResult = array(
		'order_line_id' => 1,
		'order_id' => "1",
		'product_id' => "2"
	);
	
	$actualResult = $da->convertModelToRow($o);

	if(empty(array_diff_assoc($expectedResult, $actualResult))){
		$testResults[] = "PASS - Converted Order Line to proper assoc array";
	}else{
		$testResults[] = "FAIL - DID NOT convert Order Line to proper assoc array";
	}
}


function testConvertRowToModel(){
	global $testResults, $link;

	$testResults[] = "<b>TESTING convertRowToModel()...</b>";

	$da = new OrderLineDataAccess($link);

	$row = array(
		'order_line_id' => 1,
		'order_id' => "1",
		'product_id' => "2"
	);
	
	$actualResult = $da->convertRowToModel($row);
	
	$expectedResult = new OrderLine([
		'id' => 1,
		'orderId' => "1",
		'productId' => "2"
	]);

	if($actualResult->equals($expectedResult)){
		$testResults[] = "PASS - Converted row (assoc array) to Order Line";
	}else{
		$testResults[] = "FAIL - DID NOT Convert row (assoc array) to Order Line";
	}
}


function testGetAll(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getAll()...</b>";

	$da = new OrderLineDataAccess($link);
	$actualResult = $da->getAll();
	//var_dump($actualResult); die();
}


function testGetById(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getById()...</b>";

	$da = new OrderLineDataAccess($link);
	$actualResult = $da->getById(1);
	//var_dump($actualResult); die();
}

function testInsert(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING insert()...</b>";

	$da = new OrderLineDataAccess($link);

	$options = array(
		'id' => 1,
		'orderId' => "1",
		'productId' => "2"
	);

	$o = new OrderLine($options);

	$actualResult = $da->insert($o);
	//var_dump($actualResult); die();
	
}

function testUpdate(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING update()...</b>";

	$da = new OrderLineDataAccess($link);

	$options = array(
		'id' => 1,
		'orderId' => "2",
		'productId' => "1"
	);

	$o = new OrderLine($options);

	$actualResult = $da->update($o);
	//var_dump($actualResult);die();

}

function testDelete(){
	// Note sure how we want to handle this
	// If you allow deletes then it can get messy with FK relationships
	// It might be better to set active = no
}


?>