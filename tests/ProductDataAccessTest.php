<?php
include_once("../includes/dataaccess/ProductDataAccess.inc.php");
include_once("../includes/models/Products.inc.php");
include_once("create-test-database.inc.php");


$testResults = array();

// You'll have to run all these tests for each of your data access classes
testConstructor(); // we should test the constructor, but maybe we'll skip this one in the interest of time
testConvertModelToRow();
testConvertRowToModel();
testGetAll();
testGetById();
testInsert(); 
testUpdate(); 
//testDelete(); 

echo(implode("<br>",$testResults));


function testConstructor(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING constructor...</b>";

	// TEST - create an instance of the ConcactDataAccess class
	$da = new ProductDataAccess($link);
	
	if($da){
		$testResults[] = "PASS - Created instance of ProductDataAccess";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of ProductDataAccess";
	}

	// Test - an exception should be thrown if the $link param is not a valid link
	try{
		$da = new ProductDataAccess("BLAHHHHHH");
		$testResults[] = "FAIL - Exception is NOT thrown when link param is invalid";
	}catch(Exception $e){
		$testResults[] = "PASS - Exception is thrown when link param is invalid";
	}
}

function testConvertModelToRow(){

	global $testResults, $link;

	$testResults[] = "<b>TESTING convertModelToRow()...</b>";

	$da = new ProductDataAccess($link);

	$options = array(
		'id' => 1,
		'type' => "Shirt",
		'description' => "This is a test product",
        'price' => 19.99,
        'qty' => 1
	);

	$p = new Products($options);

	$expectedResult = array(
		'product_id' => 1, 
        'product_type' => "Shirt",
        'product_description' => "This is a test product", 
        'product_price' => 19.99,
        'product_qty' => 1
	);
	
	$actualResult = $da->convertModelToRow($p);

	/*
	// This helped me to discover that I needed to convert the id to an int (with intval())
	var_dump($expectedResult);
	var_dump($actualResult);
	die();
	*/

	if(empty(array_diff_assoc($expectedResult, $actualResult))){
		$testResults[] = "PASS - Converted Product to proper assoc array";
	}else{
		$testResults[] = "FAIL - DID NOT convert Product to proper assoc array";
	}
}


function testConvertRowToModel(){
	
	global $testResults, $link;

	$testResults[] = "<b>TESTING convertModelToRow()...</b>";

	$da = new ProductDataAccess($link);

	$options = array(
		'id' => 1,
		'type' => "Shirt",
		'description' => "This is a test product",
        'price' => 19.99,
        'qty' => 1
	);

	$p = new Products($options);

	$expectedResult = array(
		'product_id' => 1, 
        'product_type' => "Shirt",
        'product_description' => "This is a test product", 
        'product_price' => 19.99,
        'product_qty' => 1
	);
	
	$actualResult = $da->convertModelToRow($p);

	if(empty(array_diff_assoc($expectedResult, $actualResult))){
		$testResults[] = "PASS - Converted Product to proper assoc array";
	}else{
		$testResults[] = "FAIL - DID NOT convert Product to proper assoc array";
	}
	
}


function testGetAll(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getAll()...</b>";

	$da = new ProductDataAccess($link);
	$actualResult = $da->getAll();

	//$testResults[] = print_r($actualResult, true);
	//var_dump($actualResult);die();
}


function testGetById(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getById()...</b>";

	$da = new ProductDataAccess($link);
	$actualResult = $da->getById(1);

	//var_dump($actualResult);die();

}

function testInsert(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING insert()...</b>";

	$da = new ProductDataAccess($link);

	$options = array(
		'id' => 1,
		'type' => "Shirt",
		'description' => "none",
		'price' => 19.99, 
		'qty' => 1
	);

	$p = new Products($options);

	$actualResult = $da->insert($p);
	//var_dump($actualResult); die();
	
}

function testUpdate(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING update()...</b>";

	$da = new ProductDataAccess($link);

	$options = array(
		'id' => 1,
		'type' => "Updated Test Product",
		'description' => "This is a test product",
        'price' => 19.99,
        'qty' => 2
	);

	$actualResult = $da->update(new Products($options));
	//var_dump($actualResult);die();
}


?>