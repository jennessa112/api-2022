<?php

// In phpMyAdmin, create a database named test_db
// and set the collation to: utf8mb4_unicode_ci

$testDB = "adv_topics_final_project";
$testServer = "localhost";
$testLogin = "root";
// NOTE that the MySQL password on the Ubuntu machine is 'test'
// AND the password on XAMPP is an empty string
// We can check the operating system of the server like so:
//die(PHP_OS); // PHP_OS is a predefined constant, it will be WINNT for Windows (XAMPP)
$testPassword = PHP_OS == "WINNT" ? "" : "test";

$link = mysqli_connect($testServer, $testLogin, $testPassword, $testDB);


if(!$link){
	die("Unable to connect to test db");
}

$dropUsersTable = "DROP TABLE IF EXISTS users;";
$dropUserRolesTable = "DROP TABLE IF EXISTS user_roles;";
$dropProductTable = "DROP TABLE IF EXISTS products";
$dropOrdersTable = "DROP TABLE IF EXISTS orders";
$dropOrderLineTable = "DROP TABLE IF EXISTS orderLine";

$createUsersTable = "
	CREATE TABLE IF NOT EXISTS users (
	  user_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	  user_first_name varchar(30) NOT NULL,
	  user_last_name varchar(30) NOT NULL,
	  user_email varchar(100) NOT NULL UNIQUE,
	  user_password char(255) NOT NULL,
	  user_salt char(32) NOT NULL,
	  user_role INT NOT NULL DEFAULT '1',
	  user_active enum('yes','no') NOT NULL DEFAULT 'yes'
	  # , FOREIGN KEY (user_role) REFERENCES user_roles(user_role_id)
	);";

$createUserRolesTable = "
	CREATE TABLE `user_roles` (
	  `user_role_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	  `user_role_name` varchar(30) NOT NULL,
	  `user_role_desc` varchar(200) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$populateUserRolesTable = "
	INSERT INTO `user_roles` (`user_role_id`, `user_role_name`, `user_role_desc`) VALUES
	(1, 'Standard User', 'Normal user with no special permissions'),
	(2, 'Admin', 'Extra permissions');";

$populateUsersTable = "
	INSERT INTO users (user_first_name,user_last_name, user_email, user_password, user_salt, user_role, user_active) VALUES 
	('John', 'Doe','john@doe.com', 'opensesame', 'xxx', '1', 'yes'),
	('Jane', 'Anderson','jane@doe.com', 'letmein', 'xxx', '2', 'yes'),
	('Bob', 'Smith','bob@smith.com', 'test', 'xxx', '2', 'yes');";	

$createProductTable = "
	CREATE TABLE IF NOT EXISTS products (
	  product_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	  product_type varchar(40) NOT NULL,
	  product_description varchar(200) NOT NULL,
	  product_price decimal NOT NULL,
	  product_qty int NOT NULL
	);";

$createOrdersTable = "
	CREATE TABLE IF NOT EXISTS orders (
		order_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
		order_status varchar(30),
		user_id INT NOT NULL DEFAULT '1'
		# , FOREIGN KEY (user_id) REFERENCES users(user_id)
	);";


$createOrderLineTable = "
	CREATE TABLE IF NOT EXISTS orderLine (
		order_line_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
		product_id INT NOT NULL DEFAULT '1',
		order_id INT NOT NULL DEFAULT '1'
		# , FOREIGN KEY (product_id) REFERENCES products(product_id)
		# , FOREIGN KEY (order_id) REFERENCES order(order_id)
	);";

$populateProductsTable = "
	INSERT INTO products (product_id, product_type, product_description, product_price, product_qty) VALUES
		(1, 'T-Shirt', 'Purple 100% cotton', '19.99', '2'),
		(2, 'Sweatpants', 'Grey 100% cotton', '39.99', '1');";

$populateOrdersTable = "
	INSERT INTO orders (order_id, user_id, order_status) VALUES 
		(1, '1','Completed'),
		(2, '2','In Progross');";

$populateOrderLineTable = "
	INSERT INTO orderline (order_line_id, product_id, order_id) VALUES 
	(1, '1','1'),
	(2, '2','2');";	

	


// Note: you have to drop the user_roles table before the users table
// because of the FK relationship
// // You also have to create and populate the user_roles table before the users table
mysqli_query($link, $createUserRolesTable) or die(mysqli_error($link));
mysqli_query($link, $populateUserRolesTable) or die(mysqli_error($link));
mysqli_query($link, $createUsersTable) or die(mysqli_error($link));
mysqli_query($link, $populateUsersTable) or die(mysqli_error($link));
\mysqli_query($link, $createProductTable) or die(mysqli_error($link));
mysqli_query($link, $createOrdersTable) or die(mysqli_error($link));
mysqli_query($link, $createOrderLineTable) or die(mysqli_error($link));
mysqli_query($link, $populateProductsTable) or die(mysqli_error($link));
mysqli_query($link, $populateOrdersTable) or die(mysqli_error($link));
mysqli_query($link, $populateOrderLineTable) or die(mysqli_error($link));