<?php
include_once("../includes/models/Products.inc.php");


$options = array(
	'id' => 1,
	'type' => "Test product",
	'description' => "This is a product for testing",
    'price' => 14.99,
    'qty' => 2
);

// This array will store the test results
$testResults = array();

// run the test functions
testConstructor();
testIsValid();


echo(implode("<br>",$testResults));



function testConstructor(){
	global $testResults, $options;
	$testResults[] = "<b>Testing constructor...</b>";

	// TEST - Make sure we can create a product object
	$p = new Products();
	
	if($p){
		$testResults[] = "PASS - Created instance of Product model object";
	}else{
		$testResults[] = "FAIL - DID NOT create instance of a Product model object";
	}

	// TEST - Make sure each property gets set correctly
	$p = new Products($options);

	if($p->id === 1){
		$testResults[] = "PASS - Set id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set id properly";
	}

	if($p->type == "Test product"){
		$testResults[] = "PASS - Set type properly";
	}else{
		$testResults[] = "FAIL - DID NOT set type properly";
	}
	
	if($p->description == "This is a product for testing"){
		$testResults[] = "PASS - Set description properly";
	}else{
		$testResults[] = "FAIL - DID NOT set description properly";
	}

    if($p->price == 14.99){
		$testResults[] = "PASS - Set price properly";
	}else{
		$testResults[] = "FAIL - DID NOT set price properly";
	}

    if($p->qty == 2){
		$testResults[] = "PASS - Set qty properly";
	}else{
		$testResults[] = "FAIL - DID NOT set qty properly";
	}

}


function testIsValid(){
	global $testResults, $options;
	$testResults[] = "<b>Testing isValid()...</b>";

	$p = new Products($options);

	// isValid() should return false when the ID is not numeric
	$p->id = "";

	if($p->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is not numeric";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is not numeric";
	}

	// isValid() should return false when the ID is a negative number
	$p->id = -1;
	if($p->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is a negative number";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is a negative number";
	}

	// If the ID is not valid, then the validation errors array should include an 'id' key
	$errors = $p->getValidationErrors();
	if(isset($errors['id'])){
		$testResults[] = "PASS - validationErrors includes key for ID";
	}else{
		$testResults[] = "FAIL - validationErrors does NOT include key for ID";
	}


	$p = new Products($options);
	$p->type = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
	if($p->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when type is too long";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when type is too long";
	}
	
	// the description must be less than 200 characters
	$p = new Products($options);
	$p->description = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                       xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                       xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                       xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

	if($p->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when Description is too long";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when Description is too long";
	}

	// When the role description is longer than 200 characters, the error message should be 'Role description must be 200 characters or less'
	$errors = $p->getValidationErrors();
	if(isset($errors['description']) && $errors['description'] == "Product description must be 200 characters or less"){
		$testResults[] = "PASS - Validation message is 'Product description must be 200 characters or less'";
	}else{
		$testResults[] = "FAIL - Validation message is NOT 'Product description must be 200 characters or less'";
	}


	// the description can be empty
	$p = new Products($options);
	$p->description = "";

	if($p->isValid() === true){
		$testResults[] = "PASS - isValid() returns true when description is empty";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return true when description is empty";
	}


	// isValid() returns true when everything is valid
	$p = new Products($options);
	
	if($p->isValid() === true){
		$testResults[] = "PASS - isValid() returns true when everything is valid";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return true when everything is valid";
	}

	// when isValid returns true, there should be no validation error
	$errors = $p->getValidationErrors();
	if(empty($errors)){
		$testResults[] = "PASS - There are no validation messages when isValid() returns true";
	}else{
		$testResults[] = "FAIL - There ARE validation messages when isValid() returns true";
	}
	
	
}
