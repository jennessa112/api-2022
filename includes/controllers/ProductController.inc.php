<?php
	include_once("Controller.inc.php");
	include_once(__DIR__ . "/../models/Products.inc.php");
	include_once(__DIR__ . "/../dataaccess/ProductDataAccess.inc.php");

	class ProductController extends Controller{

		function __construct($link){
			parent::__construct($link);
		}
		
		public function handleProducts(){

			$da = new ProductDataAccess($this->link);

			switch($_SERVER['REQUEST_METHOD']){
				case "POST":

                    $data = $this->getJSONRequestBody();
                    
                    $product = new Products($data);

                    if($product->isValid()){
                        try{
                            $product = $da->insert($product);
                            $json = json_encode($product);
                            $this->setContentType("json");
                            $this->sendHeader(200);
                            echo($json);
                            die();
                        }catch(Exception $e){
                            $this->sendHeader(400, $e->getMessage());
                            die();
                        }
                    }else{
                        $msg = implode(',', array_values($product->getValidationErrors()));
                        $this->sendHeader(406, $msg);
                        die();
                    }
					
					break;
				case "GET":
					//echo("GET ALL PRODUCTS");
					
                    $products = $da->getAll();
                    //print_r($products);die();

                    // Convert the products to json (and set the Content-Type header)
                    $jsonProducts = json_encode($products);

                    // set the headers (before echoing anything into the response body)
                    $this->setContentType("json");
                    $this->sendHeader(200);
                    
                    // set the response body
                    echo($jsonProducts);
                    die();


					break;
				case "OPTIONS":
					// AJAX CALLS WILL OFTEN SEND AN OPTIONS REQUEST BEFORE A PUT OR DELETE
					// TO SEE IF CERTAIN REQUEST METHODS WILL BE ALLOWED
					header("Access-Control-Allow-Methods: GET,POST");
					break;
				default:
					// set a 400 header (invalid request)
					$this->sendHeader(400);
			}
		}

		public function handleSingleProduct(){

			$url_path = $this->getUrlPath();
			$url_path = $this->removeLastSlash($url_path);
			
			// extracting the product id
			$id = null;		
			if(preg_match('/^products\/([0-9]*\/?)$/', $url_path, $matches)){
				$id = $matches[1];
			}

			$da = new ProductDataAccess($this->link);
			
			switch($_SERVER['REQUEST_METHOD']){
				case "GET":

                    $product = $da->getById($id);
                    $json = json_encode($product);
                    $this->setContentType("json");
                    $this->sendHeader(200);
                    echo($json);
                    die();

					break;
				case "PUT":
                    echo("Put");
                    $data = $this->getJSONRequestBody();
                    $product = new Products($data);
                    
                    //validating the product being posted
                    if($product->isValid()){
                        try{
                            if($da->update($product)){
                                $json = json_encode($product);
                                $this->setContentType("json");
                                $this->sendHeader(200);
                                echo($json);
                            }
                        }catch(Exception $e){
                            $this->sendHeader(400, $e->getMessage());	
                        }
                        die();
                    }else{
                        //die("NOT VALID");
                        $msg = implode(',', array_values($product->getValidationErrors()));
                        $this->sendHeader(406, $msg);
                        die();
                    }

					break;
                case "OPTIONS":
                    // AJAX CALLS WILL OFTEN SEND AN OPTIONS REQUEST BEFORE A PUT OR DELETE
                    // TO SEE IF THE PUT/DELETE WILL BE ALLOWED
                    header("Access-Control-Allow-Methods: GET,PUT,DELETE, POST");
                    break;
			}
		}
	}