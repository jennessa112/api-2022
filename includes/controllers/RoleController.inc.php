<?php

include_once("Controller.inc.php");
include_once(__DIR__ . "/../models/Role.inc.php");
include_once(__DIR__ . "/../dataaccess/RoleDataAccess.inc.php");

class RoleController extends Controller{

    function __construct($link){
        parent::__construct($link);
    }

    public function handleRoles(){
        
        $da = new RoleDataAccess($this->link);

        switch($_SERVER['REQUEST_METHOD']){
            case "POST":
                echo("INSERT ROLE");
                
                break;
            case "GET":
                //echo("GET ALL ROLES");
                $roles = $da->getAll();
                //print_r($roles); die();

                // Convert the users to json (and set the Content-Type header)
                $jsonRoles = json_encode($roles);

                // set the headers (before echoing anything into the response body)
                $this->setContentType("json");
                $this->sendHeader(200);
                
                // set the response body
                echo($jsonRoles);
                die();

                break;
            case "OPTIONS":
                // AJAX CALLS WILL OFTEN SEND AN OPTIONS REQUEST BEFORE A PUT OR DELETE
                // TO SEE IF THE PUT/DELETE WILL BE ALLOWED
                header("Access-Control-Allow-Methods: GET,PUT,DELETE, POST");
                break;
            default:
                // set a 400 header (invalid request)
                $this->sendHeader(400);
            
        }
    }

}