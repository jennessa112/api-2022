<?php
	include_once("Controller.inc.php");
	include_once(__DIR__ . "/../models/User.inc.php");
	include_once(__DIR__ . "/../dataaccess/UserDataAccess.inc.php");

	class UserController extends Controller{

		function __construct($link){
			parent::__construct($link);
		}
		
		public function handleUsers(){

			$da = new UserDataAccess($this->link);

			switch($_SERVER['REQUEST_METHOD']){
				case "POST":
					//echo("INSERT USER");

                    if(SECURE_SERVER_RESOURCES){
                        if(!$this->isAdmin()){
                            $this->sendHeader(401, "Only admins can insert new users");
                            die();
                        }
                    }                

                    $data = $this->getJSONRequestBody();
                    //print_r($data);die()
                    $user = new User($data);
                    //print_r($user);die();

                    if($user->isValid()){
                        try{
                            $user = $da->insert($user);
                            $json = json_encode($user);
                            $this->setContentType("json");
                            $this->sendHeader(200);
                            echo($json);
                            die();
                        }catch(Exception $e){
                            $this->sendHeader(400, $e->getMessage());
                            die();
                        }
                    }else{
                        $msg = implode(',', array_values($user->getValidationErrors()));
                        $this->sendHeader(406, $msg);
                        die();
                    }
					
					break;
				case "GET":
					//echo("GET ALL USERS");

                    if(SECURE_SERVER_RESOURCES){
                        if(!$this->isAdmin()){
                            $this->sendHeader(401, "Only admins can get all users");
                            die();
                        }
                    }
					
                    $users = $da->getAll();
                    //print_r($users);die();

                    // Convert the users to json (and set the Content-Type header)
                    $jsonUsers = json_encode($users);

                    // set the headers (before echoing anything into the response body)
                    $this->setContentType("json");
                    $this->sendHeader(200);
                    
                    // set the response body
                    echo($jsonUsers);
                    die();


					break;
				case "OPTIONS":
					// AJAX CALLS WILL OFTEN SEND AN OPTIONS REQUEST BEFORE A PUT OR DELETE
					// TO SEE IF CERTAIN REQUEST METHODS WILL BE ALLOWED
					header("Access-Control-Allow-Methods: GET,POST");
					break;
				default:
					// set a 400 header (invalid request)
					$this->sendHeader(400);
			}
		}

		public function handleSingleUser(){

			$url_path = $this->getUrlPath();
			$url_path = $this->removeLastSlash($url_path);
			//echo($url_path); die();

			// extract the user id
			$id = null;		
			if(preg_match('/^users\/([0-9]*\/?)$/', $url_path, $matches)){
				$id = $matches[1];
			}

			$da = new UserDataAccess($this->link);
			
			switch($_SERVER['REQUEST_METHOD']){
				case "GET":
					//echo("GET USER $id");

                    if(SECURE_SERVER_RESOURCES){
                        if(!$this->isAdmin() && !$this->isOwner($id)){
                            $this->sendHeader(401, "Only admins or owners can get a user (a user can get his/her own info)");
                            die();
                        }
                    }

                    $user = $da->getById($id);
                    $json = json_encode($user);
                    $this->setContentType("json");
                    $this->sendHeader(200);
                    echo($json);
                    die();

					break;
				case "PUT":
					//echo("UPDATE USER $id");

                    if(SECURE_SERVER_RESOURCES){
                        // note that admins can update any user
                        // but standard users can only update their 'own' info
                        if(!$this->isAdmin() && !$this->isOwner($id)){
                            $this->sendHeader(401, "Only admins or owners can put a user (a user can put his/her own info)");
                            die();
                        }
                    }
                    //ISSUE: Users should not be able to change their own role or active status!

                    $data = $this->getJSONRequestBody();
                    $user = new User($data);
                    
                    // make sure to validate the user being posted
                    if($user->isValid()){
                        try{
                            if($da->update($user)){
                                $json = json_encode($user);
                                $this->setContentType("json");
                                $this->sendHeader(200);
                                echo($json);
                            }
                        }catch(Exception $e){
                            $this->sendHeader(400, $e->getMessage());	
                        }
                        die();
                    }else{
                        //die("NOT VALID");
                        $msg = implode(',', array_values($user->getValidationErrors()));
                        $this->sendHeader(406, $msg);
                        die();
                    }

					break;
				case "DELETE":
					//echo("DELETE USER $id");

                    if(SECURE_SERVER_RESOURCES){
                        if(!$this->isAdmin()){
                            $this->sendHeader(401, "Only admins can delete a user");
                            die();
                        }
                    }

                    // instead of deleting the row, we'll make the user inactive
                    if($user = $da->getById($id)){
                        $user->active = "no";
                        $da->update($user);
                        $this->sendHeader(200);
                    }else{
                        $this->sendHeader(400, "Unable to 'delete' user $id");	
                    }

					break;
				case "OPTIONS":
					// AJAX CALLS WILL OFTEN SEND AN OPTIONS REQUEST BEFORE A PUT OR DELETE
					// TO SEE IF THE PUT/DELETE WILL BE ALLOWED
					header("Access-Control-Allow-Methods: GET,PUT,DELETE");
					break;
				default:
					// set a 400 header (invalid request)
					$this->sendHeader(400);
			}
		}
	}