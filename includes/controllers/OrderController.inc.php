<?php
	include_once("Controller.inc.php");
	include_once(__DIR__ . "/../models/Order.inc.php");
	include_once(__DIR__ . "/../dataaccess/OrderDataAccess.inc.php");

	class OrderController extends Controller{

		function __construct($link){
			parent::__construct($link);
		}
		
		public function handleOrders(){

			$da = new OrderDataAccess($this->link);

			switch($_SERVER['REQUEST_METHOD']){
				case "POST":
                    echo("Post Order");
                    $data = $this->getJSONRequestBody();
                    
                    $order = new Orders($data);

                    if($order->isValid()){
                        try{
                            $order = $da->insert($order);
                            $json = json_encode($order);
                            $this->setContentType("json");
                            $this->sendHeader(200);
                            echo($json);
                            die();
                        }catch(Exception $e){
                            $this->sendHeader(400, $e->getMessage());
                            die();
                        }
                    }else{
                        $msg = implode(',', array_values($order->getValidationErrors()));
                        $this->sendHeader(406, $msg);
                        die();
                    }
					
					break;
				case "GET":
					//echo("GET ALL ORDERS");
					
                    $orders = $da->getAll();
                    //print_r($orders);die();

                    // Convert the orders to json (and set the Content-Type header)
                    $jsonOrders = json_encode($orders);

                    // set the headers (before echoing anything into the response body)
                    $this->setContentType("json");
                    $this->sendHeader(200);
                    
                    // set the response body
                    echo($jsonOrders);
                    die();


					break;
				case "OPTIONS":
					// AJAX CALLS WILL OFTEN SEND AN OPTIONS REQUEST BEFORE A PUT OR DELETE
					// TO SEE IF CERTAIN REQUEST METHODS WILL BE ALLOWED
					header("Access-Control-Allow-Methods: GET,POST");
					break;
				default:
					// set a 400 header (invalid request)
					$this->sendHeader(400);
			}
		}

		public function handleSingleOrder(){

			$url_path = $this->getUrlPath();
			$url_path = $this->removeLastSlash($url_path);
			//echo($url_path); die();

			// extract the order id
			$id = null;		
			if(preg_match('/^orders\/([0-9]*\/?)$/', $url_path, $matches)){
				$id = $matches[1];
			}

			$da = new OrderDataAccess($this->link);
			
			switch($_SERVER['REQUEST_METHOD']){
				case "GET":
					//echo("GET Order $id");
                    
                    $order = $da->getById($id);
                    $json = json_encode($order);
                    $this->setContentType("json");
                    $this->sendHeader(200);
                    echo($json);
                    die();

					break;
				case "PUT":
					//echo("UPDATE Order $id");

                    $data = $this->getJSONRequestBody();
                    $order = new Orders($data);
                    
                    if($order->isValid()){
                        try{
                            if($da->update($order)){
                                $json = json_encode($order);
                                $this->setContentType("json");
                                $this->sendHeader(200);
                                echo($json);
                            }
                        }catch(Exception $e){
                            $this->sendHeader(400, $e->getMessage());	
                        }
                        die();
                    }else{
                        //die("NOT VALID");
                        $msg = implode(',', array_values($order->getValidationErrors()));
                        $this->sendHeader(406, $msg);
                        die();
                    }

					break;
                case "OPTIONS":
                    // AJAX CALLS WILL OFTEN SEND AN OPTIONS REQUEST BEFORE A PUT OR DELETE
                    // TO SEE IF THE PUT/DELETE WILL BE ALLOWED
                    header("Access-Control-Allow-Methods: GET,PUT,DELETE, POST");
                    break;
			}
		}
	}