<?php

include_once("DataAccess.inc.php");
include_once(__DIR__ . "/../models/Products.inc.php");

class ProductDataAccess extends DataAccess{

    function __construct($link){
        parent::__construct($link); //calls the super class constructor
    }

    //Converts a product model to a database row
    function convertModelToRow($product){
        $row = [];
        $row['product_id'] = intval(mysqli_real_escape_string($this->link, $product->id));
        $row['product_type'] = mysqli_real_escape_string($this->link, $product->type);
        $row['product_description'] = mysqli_real_escape_string($this->link, $product->description);
        $row['product_price'] = mysqli_real_escape_string($this->link, $product->price);
        $row['product_qty'] = mysqli_real_escape_string($this->link, $product->qty);

        return $row;
    }

    //Converts a row from the database to a role product class
    function convertRowToModel($row){
        $product = new Products();
        $product->id = intval($row['product_id']);
        $product->type = htmlentities($row['product_type']);
        $product->description = htmlentities($row['product_description']);
        $product->price = htmlentities($row['product_price']);
        $product->qty = htmlentities($row['product_qty']);

        return $product;
    }

    function getAll($args = []){
        $qStr = "SELECT 
                    product_id, 
                    product_type, 
                    product_description,
                    product_price,
                    product_qty 
                FROM products";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        $allRoles = [];

        while($row = mysqli_fetch_assoc($result)){
            $allRoles[] = $this->convertRowToModel($row);
        }

        return $allRoles;
    }

    function getById($id){
        $qStr = "SELECT 
                    product_id, 
                    product_type, 
                    product_description,
                    product_price,
                    product_qty 
                 FROM products
                 WHERE product_id =" . mysqli_real_escape_string($this->link, $id); 
 
        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        if($result->num_rows == 1){
            $row = mysqli_fetch_assoc($result);
            $product = $this->convertRowToModel($row);
            return $product;
        }

        return false;

     }

    function insert($product){

        $row = $this->convertModelToRow($product);
 
        $qStr = "INSERT INTO products (
                    product_type, 
                    product_description,
                    product_price,
                    product_qty
                ) VALUES (
                    '{$row['product_type']}',
                    '{$row['product_description']}',
                    '{$row['product_price']}',
                    '{$row['product_qty']}'
                )";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if($result){
            // add the product id that was assigned by the data base
            $product->id = mysqli_insert_id($this->link);
            return $product;
        }else{
            $this->handleError("unable to insert product");
        }

        return false;
    }

    function update($product){
        
        $row = $this->convertModelToRow($product);
               
        $qStr = "UPDATE products SET
				product_type = '{$row['product_type']}',
				product_description = '{$row['product_description']}',
				product_price = '{$row['product_price']}', 
				product_qty = '{$row['product_qty']}'
			WHERE product_id = " . $row['product_id'];
       
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		if($result){
			return true;
		}else{
			$this->handleError("unable to update product");
		}

		return false;
    }

    function delete($id){

    }

}