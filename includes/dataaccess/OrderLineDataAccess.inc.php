<?php

include_once("DataAccess.inc.php");
include_once(__DIR__ . "/../models/OrderLine.inc.php");

class OrderLineDataAccess extends DataAccess{

    function __construct($link){
        parent::__construct($link); //calls the super class constructor
    }

    //Converts a order line model to a database row
    function convertModelToRow($orderLine){
        $row = [];
        $row['order_line_id'] = intval(mysqli_real_escape_string($this->link, $orderLine->id));
        $row['product_id'] = mysqli_real_escape_string($this->link, $orderLine->productId);
        $row['order_id'] = mysqli_real_escape_string($this->link, $orderLine->orderId);
       
        return $row;
    }

    //Converts a row from the database to a order line model class
    function convertRowToModel($row){
        $orderLine = new OrderLine();
        $orderLine->id = intval($row['order_line_id']);
        $orderLine->productId = htmlentities($row['product_id']);
        $orderLine->orderId = htmlentities($row['order_id']);
        
        return $orderLine;
    }

    function getAll($args = []){
        $qStr = "SELECT 
                    order_line_id, 
                    product_id,
                    order_id 
                FROM orderline";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        $allRoles = [];

        while($row = mysqli_fetch_assoc($result)){
            $allRoles[] = $this->convertRowToModel($row);
        }

        return $allRoles;
    }

    function getById($id){
        
        $qStr = "SELECT
            order_line_id, 
            product_id, 
            order_id
        FROM orderline
        WHERE order_line_id = " . mysqli_real_escape_string($this->link, $id);

        //die($qStr);

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if($result->num_rows == 1){

            $row = mysqli_fetch_assoc($result);
            $orderLine = $this->convertRowToModel($row);
            return $orderLine;
        }

        return false;

     }

    function insert($orderLine){
        $row = $this->convertModelToRow($orderLine);
        $qStr = "INSERT INTO orderline (
                    product_id, 
                    order_id
        ) VALUES (
            '{$row['product_id']}',
            '{$row['order_id']}'
        )";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        if($result){
            $orderLine->id = mysqli_insert_id($this->link);
            return $orderLine;
        }else{
            $this->handleError("Unable to insert order line");
        }

        return false;
    }

    function update($orderLine){

        $row = $this->convertModelToRow($orderLine);
               
        $qStr = "UPDATE orderline SET
				order_line_id = '{$row['order_line_id']}',
				product_id = '{$row['product_id']}',
				order_id = '{$row['order_id']}'
			WHERE order_line_id = " . $row['order_line_id'];
       
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		if($result){
			return true;
		}else{
			$this->handleError("unable to update order line");
		}

		return false;
    }

    function delete($id){

    }

}