<?php

include_once("DataAccess.inc.php");
include_once(__DIR__ . "/../models/Order.inc.php");

class OrderDataAccess extends DataAccess{

    function __construct($link){
        parent::__construct($link); //calls the super class constructor
    }


    function convertModelToRow($order){

		$row = [];
		$row['order_id']	= mysqli_real_escape_string($this->link, $order->id);
		$row['order_status'] = mysqli_real_escape_string($this->link, $order->status);
		$row['user_id'] = mysqli_real_escape_string($this->link, $order->userId);
		
		return $row;
	}

	function convertRowToModel($row){
		// Note that if you have a column that allows some HMTL content
		// then use $this->sanitizeHTML() instead of htmlentities()

		$order = new Orders();
		$order->id = htmlentities($row['order_id']);
		$order->status = htmlentities($row['order_status']);
		$order->userId = htmlentities($row['user_id']);

		return $order;
	}

    function getAll($args = []){
        $qStr = "SELECT 
                    order_id, 
                    order_status, 
                    user_id
                FROM orders";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        $allRoles = [];

        while($row = mysqli_fetch_assoc($result)){
            $allRoles[] = $this->convertRowToModel($row);
        }

        return $allRoles;
    }

    function getById($id){
        $qStr = "SELECT
		            order_id, 
		            order_status, 
		            user_id 
		        FROM orders
		        WHERE order_id = " . mysqli_real_escape_string($this->link, $id);
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));
		
		if($result->num_rows == 1){

		    $row = mysqli_fetch_assoc($result);
		    $order = $this->convertRowToModel($row);
		    return $order;
		}

		return false;

     }

    function insert($order){
        		        
        $row = $this->convertModelToRow($order);
    
        $qStr = "INSERT INTO orders (
                    order_id,
                    order_status, 
                    user_id
                ) VALUES (
                    '{$row['order_id']}',
                    '{$row['order_status']}',
                    '{$row['user_id']}'
                )";


        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if($result){
            $order->id = mysqli_insert_id($this->link);
            return $order;
        }else{
            $this->handleError("unable to insert order");
        }

        return false;
    }

    function update($orders){
        $row = $this->convertModelToRow($orders);
        $qStr = "UPDATE orders SET
                    order_status = '{$row['order_status']}',
                    user_id = '{$row['user_id']}'
                WHERE order_id = ". $row['order_id'];

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        if($result){
            return true;
        }else{
            $this->handleError("Unable to update order");
        }

        return false;
    }

    function delete($id){

    }

}