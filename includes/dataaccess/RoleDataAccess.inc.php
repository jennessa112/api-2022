<?php

include_once("DataAccess.inc.php");
include_once(__DIR__ . "/../models/Role.inc.php");

class RoleDataAccess extends DataAccess{

    function __construct($link){
        parent::__construct($link); //calls the super class constructor
    }

    //Converts a role model to a database row
    function convertModelToRow($role){
        $row = [];
        $row['user_role_id'] = intval(mysqli_real_escape_string($this->link, $role->id));
        $row['user_role_name'] = mysqli_real_escape_string($this->link, $role->name);
        $row['user_role_desc'] = mysqli_real_escape_string($this->link, $role->description);

        return $row;
    }

    //Converts a row from the database to a role model class
    function convertRowToModel($row){
        $role = new Role();
        $role->id = intval($row['user_role_id']);
        $role->name = htmlentities($row['user_role_name']);
        $role->description = htmlentities($row['user_role_desc']);
        return $role;
    }

    function getAll($args = []){
        $qStr = "SELECT 
                    user_role_id, 
                    user_role_name, 
                    user_role_desc 
                FROM user_roles";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        $allRoles = [];

        while($row = mysqli_fetch_assoc($result)){
            $allRoles[] = $this->convertRowToModel($row);
        }

        return $allRoles;
    }

    function getById($id){
        $qStr = "SELECT 
                     user_role_id, 
                     user_role_name, 
                     user_role_desc 
                 FROM user_roles
                 WHERE user_role_id =" . mysqli_real_escape_string($this->link, $id); 
 
        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        if($result->num_rows == 1){
            $row = mysqli_fetch_assoc($result);
            $role = $this->convertRowToModel($row);
            return $role;
        }

        return false;

     }

    function insert($role){
        $row = $this->convertModelToRow($role);
        $qStr = "INSERT INTO user_roles (
            user_role_name,
            user_role_desc
        ) VALUES (
            '{$row['user_role_name']}',
            '{$row['user_role_desc']}'
        )";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        if($result){
            $role->id = mysqli_insert_id($this->link);
            return $role;
        }else{
            $this->handleError("Unable to insert role");
        }

        return false;
    }

    function update($role){
        $row = $this->convertModelToRow($role);
        $qStr = "UPDATE user_roles SET
                    user_role_name = '{$row['user_role_name']}',
                    user_role_desc = '{$row['user_role_dsec']}'
                WHERE user_role_id = ". $row['user_role_id'];

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->$link));

        if($result){
            return true;
        }else{
            $this->handleError("Unable to update role");
        }

        return false;
    }

    function delete($id){

    }

}