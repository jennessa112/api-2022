<?php

include_once("Model.inc.php");

class OrderLine extends Model
{

    // INSTANCE VARIABLES
    public $id;
    public $productId;
    public $orderId;

    //Constructor for creating Order Line model objects
    public function __construct($args = []){

        $this->id = $args['id'] ?? 0;
        $this->productId = $args['productId'] ?? "";
        $this->orderId = $args['orderId'] ?? "";
       
    }

  function isValid(){
       
        $valid = true;
        $this->validationErrors = [];
       
        // validate id
        if (!is_numeric($this->id) || !($this->id >= 0)) {
            $valid = false;
            $this->validationErrors['id'] = "ID is not valid";
        }

        if (!($this->productId === 1 || $this->productId === 2)) {
			$valid = false;
			$this->validationErrors['productId'] = "The product id is not valid";
		}

        if (!($this->orderId === 1 || $this->orderId === 2)) {
			$valid = false;
			$this->validationErrors['orderId'] = "The order id is not valid";
		}

        return $valid;
    }




}