<?php

include_once("Model.inc.php");

class Products extends Model
{

    // INSTANCE VARIABLES
    public $id;
    public $type;
    public $description;
    public $price;
    public $qty;

    //Constructor for creating Products model objects
    public function __construct($args = []){

        $this->id = $args['id'] ?? 0;
        $this->type = $args['type'] ?? "";
        $this->description = $args['description'] ?? "";
        $this->price = $args['price'] ?? "";
        $this->qty = $args['qty'] ?? "";
       
    }

  function isValid(){
       
    $valid = true;
    $this->validationErrors = [];
    
    // validate id
    if (!is_numeric($this->id) || !($this->id >= 0)) {
        $valid = false;
        $this->validationErrors['id'] = "ID is not valid";
    }

    // type should be 30 characters or less
    if (empty($this->type)) {
        $valid = false;
        $this->validationErrors['type'] = "Product type is required";
    }else if(strlen($this->type) > 30){
        $valid = false;
        $this->validationErrors['type'] = "Product type must be 30 characters or less";
    }

    // description should be 200 characters or less
    // But it is not required
    if(strlen($this->description) > 200){
        $valid = false;
        $this->validationErrors['description'] = "Product description must be 200 characters or less";
    }

    if (empty($this->price)) {
            $valid = false;
            $this->validationErrors['price'] = "Product price is required";
    }

    if (empty($this->qty)) {
            $valid = false;
            $this->validationErrors['qty'] = "Product qty is required";
        }


        return $valid;
    }

}