<?php

include_once("Model.inc.php");

class Orders extends Model
{

    // INSTANCE VARIABLES
    public $id;
    public $status;
    public $userId;

    //Constructor for creating Order model objects
    public function __construct($args = []){

        $this->id = $args['id'] ?? 0;
        $this->status = $args['status'] ?? "";
        $this->userId = $args['userId'] ?? "";
       
    }
    
    //Validation
    function isValid(){
        
        $valid = true;
        $this->validationErrors = [];
    
        // validate id
        if (!is_numeric($this->id) || !($this->id >= 0)) {
            $valid = false;
            $this->validationErrors['id'] = "ID is not valid";
        }

        // status should be 30 characters or less
		if (empty($this->status)) {
			$valid = false;
			$this->validationErrors['status'] = "Order status is required";
		}else if(strlen($this->status) > 30){
			$valid = false;
			$this->validationErrors['status'] = "Order status must be 30 characters or less";
		}

        if (empty($this->userId)) {
			$valid = false;
			$this->validationErrors['userId'] = "The user is not valid";
		}
        return $valid;
    }

}