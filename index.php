<?php
//session_start(); // we'll use sessions to securing resources, although I'm not sure we should

// We need to discuss URL rewriting before we do this

include_once("includes/config.inc.php");
include_once("includes/Router.inc.php");

// Get the requested url path (thanks to the magic of mod_rewrite)
// Note that the .htaccess file is configured to redirect to this page
// and append the requested path to the query string, for example: index.php?url_path=users/1
$url_path = $_GET['url_path'] ?? "";
//die("REQUEST METHOD: " . $_SERVER['REQUEST_METHOD'] . "<br>URL:" . $url_path);

$routes = [
    "users/" => ["controller" => "UserController", "action" => "handleUsers"],
    "users/:id" => ["controller" => "UserController", "action" => "handleSingleUser"],
    "roles/" => ["controller" => "RoleController", "action" => "handleRoles"],
    "products/" => ["controller" => "ProductController", "action" => "handleProducts"],
    "products/:id" => ["controller" => "ProductController", "action" => "handleSingleProduct"],
    "orders/" => ["controller" => "OrderController", "action" => "handleOrders"],
    "orders/:id" => ["controller" => "OrderController", "action" => "handleSingleOrder"],
    "orderLine/" => ["controller" => "OrderLineController", "action" => "handleOrderLine"],
    "login/" =>	["controller" => "LoginController", "action" => "handleLogin"],
    "logout/" => ["controller" => "LoginController", "action" => "handleLogout"]
];

$router = new Router($routes);

if($route = $router->getController($url_path)){
    $className = $route['controller'];
    $methodName = $route['action'];
    //die("Instantiate $className and invoke $methodName");

    include_once("includes/controllers/$className.inc.php");
    $controller = new $className(get_link());
    call_user_func(array($controller, $methodName));
}else{
    //header('HTTP/1.1 400 Bad Request');
    header('HTTP/1.1 404 Route Not Found');
}
die();

?>